using System;
using System.Linq;
using System.Threading.Tasks;
using CatalogAPI.Infrastructure.DTOs;
using CatalogAPI.Infrastructure.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CatalogAPI
{
    [ApiController]
    [Route("api/item")]
    public class ItemController : ControllerBase 
    {
        private readonly IItemRepository _itemRepository;

        public ItemController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        [HttpGet]
        public async Task<ActionResult<ItemDTO>> GetItems()
        {
            try
            {
                var items = await _itemRepository.GetItemsAsync();

                if (items.Count() > 0)
                    	return Ok(items);
                
                return NotFound();
            }
            catch(Exception ex)
            {
                throw ex;    
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ItemDTO>> GetItemById(Guid id)
        {
            try
            {
                var item = await _itemRepository.GetItemByIdAsync(id);

                if (item is not null) 
                    return Ok(item);
                
                return NotFound();
            }
            catch(Exception ex)
            {
                throw ex;    
            }
        }

        [HttpPost]
        public async Task<ActionResult<ItemDTO>> CreateItem(ItemPostDTO item)
        {
            try
            {
                var itemDTO = await _itemRepository.CreateItemAsync(item);

                return CreatedAtAction(nameof(GetItemById), new { id = itemDTO.Id }, itemDTO);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateItem(Guid id, ItemPutDTO item)
        {
            try
            {
                if (await _itemRepository.GetItemByIdAsync(id) is null)
                    return NotFound();
                
                await _itemRepository.UpdateItemAsync(id, item);

                return NoContent();
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteItem(ItemDeleteDTO item)
        {
            try
            {
                if (await _itemRepository.GetItemByIdAsync(item.Id) is null)
                    return NotFound();
                    
                await _itemRepository.DeleteItemAsync(item);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

    }
}
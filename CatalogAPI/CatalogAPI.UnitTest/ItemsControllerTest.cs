using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CatalogAPI.Infrastructure.DTOs;
using CatalogAPI.Infrastructure.Models;
using CatalogAPI.Infrastructure.Repository;
using DnsClient.Internal;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace CatalogAPI.UnitTest
{
    public class UnitTest1
    {
        private readonly Mock<IItemRepository> repositoryStub = new();
        private readonly Random rand = new();
        
        // Convention: UnitOfWork_StateUnderTest_ExpectedBehavior()
        
        // GET ONE
        [Fact]
        public async Task GetItemById_WithUnexistingItem_ReturnsNotFound()
        {
            // Arrange: Setting up all to be re ady to test.
            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync((ItemDTO)null);

            //var loggerStub = new Mock<ILogger<ItemController>>();
            var controller = new ItemController(repositoryStub.Object);
            
            // Act: Perform the action to test.
            var result = await controller.GetItemById(Guid.NewGuid());

            // Assert: Verify the execution of the Act.
            //Assert.IsType<NotFoundResult>(result.Result); 
            result.Result.Should().BeOfType<NotFoundResult>();
        }

        // GET ONE
        [Fact]
        public async Task GetItemById_WithExistingItem_ReturnsExpectedItem()
        {
            // Arrange: Setting up all to be re ady to test.
            var expectedItem = CreateRandomItem();

            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(expectedItem);    

            var controller = new ItemController(repositoryStub.Object);
            
            // Act: Perform the action to test.
            var result = await controller.GetItemById(Guid.NewGuid());

            var okObjResult = result.Result as OkObjectResult;
            
            // Assert: Verify the execution of the Act.
            okObjResult?.Value.Should().BeOfType<ItemDTO>();
            
            okObjResult?.Value.Should().Be(expectedItem);
        }

        // GET ALL
        [Fact]
        public async Task GetItems_WithExistingItems_ReturnsAllItems()
        {
            // Arrange
            var expectedItems = new[] { CreateRandomItem(), CreateRandomItem(), CreateRandomItem() };
            
            repositoryStub.Setup(repo => repo.GetItemsAsync())
                .ReturnsAsync(expectedItems);
            
            var controller = new ItemController(repositoryStub.Object);
            
            // Act
            var actualItems = await controller.GetItems();
            
            var resultItems = actualItems.Result as OkObjectResult;
            
            // Assert
            resultItems?.Value
                .Should().BeEquivalentTo(
                expectedItems,
                options => options.ComparingByMembers<ItemDTO>());
        }

        // POST
        [Fact]
        public async Task CreateItem_WithItemToCreate_ReturnsCreatedItem()
        {
            // Arrange
            var itemToCreate = new ItemPostDTO()
            {
                Name = Guid.NewGuid().ToString(),
                Price = rand.Next(1000)
            };
            
            var controller = new ItemController(repositoryStub.Object);

            // Act
            var result = await controller.CreateItem(itemToCreate);
            
            // Assert
            var createdItemDto = result.Result as CreatedAtActionResult;

            createdItemDto?.Value
                .Should().BeEquivalentTo(
                    itemToCreate,
                    options => options.ComparingByMembers<ItemDTO>().ExcludingMissingMembers());
        }

        // UPDATE 
        [Fact]
        public async Task UpdateItem_WithExistingItem_ReturnsNoContent()
        {
            // Arrange
            var existingItem = CreateRandomItem();

            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(existingItem);

            var ItemId = existingItem.Id;
            var itemToUpdate = new ItemPutDTO()
            {
                Name = Guid.NewGuid().ToString(),
                Price = existingItem.Price + 3
            };

            var controller = new ItemController(repositoryStub.Object);

            // Act
            var result = await controller.UpdateItem(existingItem.Id, itemToUpdate);

            // Assert
            result.Should().BeOfType<NoContentResult>();
        }

        // UPDATE 
        [Fact]
        public async Task UpdateItem_WithNotExistingItem_ReturnsNotFound()
        {
            // Arrange
            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync((ItemDTO)null);

            var controller = new ItemController(repositoryStub.Object);

            // Act
            var result = await controller.GetItemById(Guid.NewGuid());

            // Assert
            result.Result.Should().BeOfType<NotFoundResult>();
        }

        // DELETE 
        [Fact]
        public async Task DeleteItem_WithExistingItem_ReturnsNoContent()
        {
            // Arrange
            var existingItem = CreateRandomItem();

            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(existingItem);

            var itemToDelete = new ItemDeleteDTO()
            {
                Id = existingItem.Id
            };

            var controller = new ItemController(repositoryStub.Object);

            // Act
            var result = await controller.DeleteItem(itemToDelete);

            // Assert
            result.Should().BeOfType<NoContentResult>();
        }

        // DELETE 
        [Fact]
        public async Task DeleteItem_WithNotExistingItem_ReturnsNotFound()
        {
            // Arrange
            repositoryStub.Setup(repo => repo.GetItemByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync((ItemDTO)null);

            var itemToDelete = new ItemDeleteDTO()
            {
                Id = Guid.NewGuid()
            };

            var controller = new ItemController(repositoryStub.Object);

            // Act
            var result = await controller.DeleteItem(itemToDelete);

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }


            #region Helper Functions

        private ItemDTO CreateRandomItem()
        {
            return new()
            {
                Id = Guid.NewGuid(),
                Name = Guid.NewGuid().ToString(),
                Price = rand.Next(1000),
                CreatedDate = DateTimeOffset.UtcNow
            };
        }
        #endregion
    }
}
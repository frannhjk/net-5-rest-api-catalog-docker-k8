﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CatalogAPI.Infrastructure.DTOs;
using CatalogAPI.Infrastructure.Models;
using Mapster;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CatalogAPI.Infrastructure.Repository
{
    public class MongoDBItemRepository : IItemRepository
    {
        private const string databaseName = "catalog";

        private const string collectionName = "items";

        private readonly IMongoCollection<Item> itemsCollection;

        private readonly FilterDefinitionBuilder<Item> filterBuilder = Builders<Item>.Filter;


        public MongoDBItemRepository(IMongoClient mongoClient)
        {
            IMongoDatabase database = mongoClient.GetDatabase(databaseName);
            itemsCollection = database.GetCollection<Item>(collectionName);
        }

        public async Task<ItemDTO> CreateItemAsync(ItemPostDTO item)
        {
            var entity = item.Adapt<Item>();

            await itemsCollection.InsertOneAsync(entity);

            return entity.Adapt<ItemDTO>();
        }

        public async Task DeleteItemAsync(ItemDeleteDTO item)
        {
            var filter = filterBuilder.Eq(existingItem => existingItem.Id, item.Id);

            //var itemEntity = item.Adapt<Item>();

            await itemsCollection.DeleteOneAsync(filter);
        }

        public async Task<ItemDTO> GetItemByIdAsync(Guid id)
        {
            var filter = filterBuilder.Eq(item => item.Id, id);

            var entity = await itemsCollection.Find(filter)
                .SingleOrDefaultAsync();

            return entity.Adapt<ItemDTO>();
        }

        public async Task<IEnumerable<ItemDTO>> GetItemsAsync()
        {
            var itemsEntities = await itemsCollection.Find(new BsonDocument())
                .ToListAsync();

            return itemsEntities.Adapt<List<ItemDTO>>();
        }

        public async Task UpdateItemAsync(Guid id, ItemPutDTO item)
        {
            var filter = filterBuilder.Eq(existingItem => existingItem.Id, id);

            var entity = await itemsCollection.Find(filter)
                .SingleOrDefaultAsync();
            
            Item updatedItem = entity with
            {
                Name = item.Name,
                Price = item.Price
            };

            await itemsCollection.ReplaceOneAsync(filter, updatedItem);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CatalogAPI.Infrastructure.DTOs;
using CatalogAPI.Infrastructure.Models;
using Mapster;

namespace CatalogAPI.Infrastructure.Repository
{
    public class ItemRepository : IItemRepository
    {
        private readonly List<Item> Items = new() // Target Type Expression C# 9
        {
            new Item { Id = Guid.NewGuid(), Name = "Potion", Price = 10, CreatedDate = DateTimeOffset.UtcNow },
            new Item { Id = Guid.NewGuid(), Name = "Sword", Price = 20, CreatedDate = DateTimeOffset.UtcNow },
            new Item { Id = Guid.NewGuid(), Name = "Shield", Price = 15, CreatedDate = DateTimeOffset.UtcNow }
        };

        

        public ItemDTO GetItemById(Guid id)
        {
            var item = Items
                .Where(item => item.Id == id)
                .SingleOrDefault();

            return item.Adapt<ItemDTO>();
        }

        public ItemDTO CreateItem(ItemPostDTO item)
        {
            var entity = item.Adapt<Item>();

            entity.Id = Guid.NewGuid();

            Items.Add(entity);

            var dtoToReturn = entity.Adapt<ItemDTO>();

            return dtoToReturn;
        }

        public void UpdateItem(Guid id, ItemPutDTO item)
        {
            var index = Items.FindIndex(exists => exists.Id == id);

            Items[index] = item.Adapt<Item>();

            var hola = Items[index];
        }

        public void DeleteItem(ItemDeleteDTO item)
        {
            var index = Items.FindIndex(exists => exists.Id == item.Id);

            Items.RemoveAt(index);
        }
        
        public async Task<IEnumerable<ItemDTO>> GetItemsAsync()
        {
            var itemEntities = await Task.FromResult(Items);
            
            var destObject = itemEntities.Adapt<IEnumerable<ItemDTO>>();

            return destObject;
        }

        public Task<ItemDTO> GetItemByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ItemDTO> CreateItemAsync(ItemPostDTO item)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItemAsync(Guid id, ItemPutDTO item)
        {
            throw new NotImplementedException();
        }

        public Task DeleteItemAsync(ItemDeleteDTO item)
        {
            throw new NotImplementedException();
        }
    }
}

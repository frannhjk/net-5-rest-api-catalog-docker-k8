﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CatalogAPI.Infrastructure.DTOs;

namespace CatalogAPI.Infrastructure.Repository
{
    public interface IItemRepository
    {
        Task<IEnumerable<ItemDTO>> GetItemsAsync();

        Task<ItemDTO> GetItemByIdAsync(Guid id);

        Task<ItemDTO> CreateItemAsync(ItemPostDTO item);

        Task UpdateItemAsync(Guid id, ItemPutDTO item);

        Task DeleteItemAsync(ItemDeleteDTO item);
    }
}

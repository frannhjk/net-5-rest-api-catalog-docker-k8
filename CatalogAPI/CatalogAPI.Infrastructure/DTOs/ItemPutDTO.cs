﻿using System;

namespace CatalogAPI.Infrastructure.DTOs
{
    public record ItemPutDTO
    {
        public String Name { get; set; }
        public decimal Price { get; set; }
    }
}
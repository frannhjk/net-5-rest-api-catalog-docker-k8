﻿using System;

namespace CatalogAPI.Infrastructure.DTOs
{
    public record ItemDeleteDTO
    {
        public Guid Id { get; set; }
    }
}
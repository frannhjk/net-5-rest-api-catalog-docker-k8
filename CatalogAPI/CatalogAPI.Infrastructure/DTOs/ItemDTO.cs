﻿using System;
namespace CatalogAPI.Infrastructure.DTOs
{
    public record ItemDTO
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public decimal Price { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}

﻿using System;
namespace CatalogAPI.Infrastructure.DTOs
{
    public record ItemPostDTO
    {
        public String Name { get; set; }
        public decimal Price { get; set; }
    }
}
